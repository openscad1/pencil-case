include <pencil-case.scad>;

width_factor = 3;
do_cap = true;
do_base = false;

offset = tube_wall_thickness / 4;

// magnet_pocket = false;


module tri_cap() {
    difference() {
        union() {
            for (a = [0, 120, 240]) {
                rotate([0, 0, a]) {
                    translate([((tube_width + (tube_wall_thickness * 2)) / 2) + offset, 0, 0]) {
                        translate([0, (tube_width + (tube_wall_thickness * 2)) / 2, 0]) {
                            render(1);
                        }
                    }
                }
            }
        }

        if(magnet_pocket) {
            translate([0, 0, tube_height - (inch * 0.5) + 0.01]) {
                magnet_slug();
            }
        }

    }
}


// debug = true;
//debug2 = true;
debug2 = false;

intersection() {
    tri_cap();
    if(debug) {
        translate([0, 0, 80]) {
            cylinder(d = 20, h = 120);
        }
    }
    if(debug2) {
        translate([0, 0, tube_height]) {
            cube([200, 200, 20], center = true);
        }
    }
}



// EOF
